package ru.ilinovsg.tm;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyServiceTest {
    private MyService myService;

    @BeforeEach
    public void setService() {
        myService = new MyService();
    }

    @Test
    void sumCorrect() {
        assertEquals(4, myService.sum("1", "3"));
    }

    @Test
    void sumException() {
        assertThrows(IllegalArgumentException.class, () -> myService.sum("str", "str"));
    }

    @Test
    void sumExceptionResultOverflow() {
        assertThrows(IllegalArgumentException.class, () -> myService.sum("9223372036854775808", "1"));
    }

    @Test
    public void factorialCorrect() {
        assertEquals(6, myService.factorial("3"));
    }

    @Test
    public void factorialIsNull() {
        assertEquals(0, myService.factorial("0"));
    }

    @Test
    void factorialExceptionNotPositive() {
        assertThrows(IllegalArgumentException.class, () -> myService.factorial("-1"));
    }

    @Test
    void factorialExceptionWrongFormat() {
        assertThrows(IllegalArgumentException.class, () -> myService.factorial("dfgh"));
    }

    @Test
    void factorialExceptionResultOverflow() {
        assertThrows(ArithmeticException.class, () -> myService.factorial("99"));
    }

    @Test
    public void fibonacciCorrect() {
        long[] expected = {1, 1, 2, 3, 5, 8, 13};
        long[] result = myService.fibonacci("13");
        assertArrayEquals(expected,result);
    }

    @Test
    public void fibonacciIsNull() {
        long[] expected1 = {0};
        long[] result1 = myService.fibonacci("0");
        assertArrayEquals(expected1,result1);
    }

    @Test
    void fibonacciExceptionNotPositive() {
        assertThrows(IllegalArgumentException.class, () -> myService.fibonacci("-1"));
    }

    @Test
    void fibonacciExceptionWrongFormat() {
        assertThrows(IllegalArgumentException.class, () -> myService.fibonacci("1 1"));
    }

    @Test
    void fibonacciExceptionIsNotFib() {
        assertThrows(IllegalArgumentException.class, () -> myService.fibonacci("11"));
    }

    @Test
    void fibonacciExceptionResultOverflow() {
        assertThrows(ArithmeticException.class, () -> myService.fibonacci("9223372036854775807"));
    }


}