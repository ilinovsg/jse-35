package ru.ilinovsg.tm;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int i = 0;

        MyService myService = new MyService();

        System.out.println(myService.sum("1", "3"));

        Long startTime = System.currentTimeMillis();
        System.out.println(myService.factorial("1000", 1));
        System.out.println("Time:" + (System.currentTimeMillis() - startTime));

        Long startTime1 = System.currentTimeMillis();
        System.out.println(myService.factorial("1000", 10));
        System.out.println("Time:" + (System.currentTimeMillis() - startTime1));

        Long startTime2 = System.currentTimeMillis();
        System.out.println(myService.factorialStreamedParallel(1000));
        System.out.println("Time:" + (System.currentTimeMillis() - startTime2));

        for (Long item : myService.fibonacci("13")) {
            System.out.print(item+" ");
        }
    }
}
