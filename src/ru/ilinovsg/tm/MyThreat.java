package ru.ilinovsg.tm;

import java.math.BigInteger;

public class MyThreat extends Thread {

    private long start;
    private long end;
    private BigInteger result;

    public MyThreat(long start, long end, BigInteger result) {
        this.start = start;
        this.end = end;
        this.result = result;
    }

    public BigInteger getResult() {
        return result;
    }

    public void setResult(BigInteger result) {
        this.result = result;
    }

    @Override
    public void run() {
        for (int i = (int) this.start; i <= (int) this.end; i++) {
            result = result.multiply(new BigInteger("" + i));
        }
    }
}
