package ru.ilinovsg.tm;

import java.math.BigInteger;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<BigInteger> {

    private long start;
    private long end;
    private BigInteger result;

    public MyCallable(long start, long end, BigInteger result) {
        this.start = start;
        this.end = end;
        this.result = result;
    }

    @Override
    public BigInteger call() throws Exception {
        for (int i = (int) this.start; i <= (int) this.end; i++) {
            result = result.multiply(new BigInteger("" + i));
        }
        return result;
    }
}
