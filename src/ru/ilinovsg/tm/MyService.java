package ru.ilinovsg.tm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class MyService {
    public long sum(String arg1, String arg2) throws IllegalArgumentException {
        return Long.parseLong(arg1) + Long.parseLong(arg2);
    }

    public BigInteger factorial(String arg, int threadsCount) throws InterruptedException {
        long argL = Long.parseLong(arg);
        ExecutorService executor = Executors.newFixedThreadPool(threadsCount);
        List<Future<BigInteger>> list = new ArrayList<Future<BigInteger>>();
        BigInteger result = BigInteger.valueOf(1);
        for (int i = 1; i <= threadsCount; i++) {
            long start = 1 + (argL / threadsCount)*(i - 1);
            long end = start + ((argL / threadsCount) - 1);
            Callable<BigInteger> callable = new MyCallable(start, end, result);
            Future<BigInteger> future = executor.submit(callable);
            list.add(future);
        }
       for(Future<BigInteger> fut : list) {
            try {
                result = result.multiply(fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdown();
        return result;
    }

    public BigInteger factorialStreamedParallel(int n) {
        if(n < 2) return BigInteger.valueOf(1);
        return IntStream.rangeClosed(2, n).parallel().mapToObj(BigInteger::valueOf).reduce(BigInteger::multiply).get();
    }

    public long[] fibonacci(String arg) throws IllegalArgumentException, ArithmeticException {
        if (Long.parseLong(arg) < 0) {
            throw new IllegalArgumentException();
        }
        Math.incrementExact(Long.parseLong(arg));
        List<Long> list = new ArrayList<>();
        if (Long.parseLong(arg) == 0) {
            list.add(Long.parseLong(arg));
        }
        else {
            long n0 = 1, n1 = 1, n2;
            int index = 2;
            list.add(n0);
            list.add(n1);
            while((n0 + n1) <= Long.parseLong(arg)) {
                n2 = Math.addExact(n0, n1);
                n0 = n1;
                n1 = n2;
                if ((n0 + n1) > Long.parseLong(arg) &&
                     n2 != Long.parseLong(arg)) {
                    throw new IllegalArgumentException();
                }
                list.add(n2);
                index++;
            }
        }
        return  list.stream().mapToLong(l -> l).toArray();
    }
}
